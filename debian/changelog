libemail-address-list-perl (0.06-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Jenkins ]
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 01:21:02 +0000

libemail-address-list-perl (0.06-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.06:
    Changes to address CVE-2018-18898 which could allow DDoS-type attacks.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.
  * Add patch to fix configure failure without '.' in @INC.
  * Mention module name in long description.

 -- gregor herrmann <gregoa@debian.org>  Wed, 06 Feb 2019 19:39:41 +0100

libemail-address-list-perl (0.05-1) unstable; urgency=medium

  * Team upload.
  * debian/copyright: update Upstream-Contact, add Comment to upstream
    copyright.
  * New upstream release.
  * Drop fix-spelling-error-in-manpage.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 00:26:08 +0100

libemail-address-list-perl (0.04-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 0.04
  * Add fix-spelling-error-in-manpage.patch patch.
    Fix spelling errors in manpage reported by lintian.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 16 Feb 2014 16:57:51 +0100

libemail-address-list-perl (0.03-1) unstable; urgency=high

  * New upstream release. This fixes a DoS vulnerability
    (CVE-2014-1474)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 23 Jan 2014 21:41:04 +0000

libemail-address-list-perl (0.01-1) unstable; urgency=low

  * Initial Release.

 -- Bastian Blank <bastian.blank@credativ.de>  Fri, 03 Jan 2014 16:24:05 +0000
